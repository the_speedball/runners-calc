import React from 'react';
import { Button, StyleSheet, Text, View, TextInput, Picker } from 'react-native';

import { calcAvg } from './TimeCalc';


class TimeInput extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.props.handleTextChange(e);
    }

    render() {
        const value = this.props.value;
        return <TextInput maxLength={2}
        style={{fontSize: 26}}
            keyboardType={'numeric'}
            value={value}
            onChangeText={this.handleChange}
            onEndEditing={(e) => {
                let v = e.nativeEvent.text;
                if (isNaN(v)) v = '0';
                this.handleChange(v.padStart(2, 0));
            }
            }
        />;
    }
}

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.handleHoursChange = this.handleHoursChange.bind(this);
        this.handleMinutesChange = this.handleMinutesChange.bind(this);
        this.handleSecondsChange = this.handleSecondsChange.bind(this);
        this.state = {
            distance: '10',
            hours: '00',
            minutes: '50',
            seconds: '00',
        };
    }

    handleHoursChange(time) {
        this.setState({ hours: time });
    }
    handleMinutesChange(time) {
        this.setState({ minutes: time });
    }

    handleSecondsChange(time) {
        this.setState({ seconds: time });
    }

    render() {
        const pace = calcAvg(this.state.distance, `${this.state.hours}:${this.state.minutes}:${this.state.seconds}`);

        return (

            <View style={styles.container}>

                <View style={{width: 350, alignItems: 'flex-start', flexDirection: 'row'}}>
                <View style={{width: 75, alignItems: 'flex-start'}}><Text style={{fontSize: 26}}>Pace:</Text></View>
                <View style={{width: 250, alignItems: 'center'}}><Text style={{fontSize: 26}}>{pace}</Text></View>
                </View>

                <View style={styles.distance}>
                <View style={{ flexDirection: 'row', width: 350, justifyContent: 'center'}}>
                    <Button title="5 km" onPress={() => this.setState({ distance: "5" })} />
                    <Button title="10 km" onPress={() => this.setState({ distance: "10" })} />
                    <Button title="15 km" onPress={() => this.setState({ distance: "15" })} />
                    <Button
                        title="Half"
                        onPress={() => this.setState({ distance: "21.0975" })}
                    />
                <Button title="Full" onPress={() => this.setState({ distance: "42.195" })} />
                </View>
                </View>

                <View>
                    <View style={styles.inputs}>
                <View style={{width: 100, alignItems: 'flex-start'}}><Text style={{fontSize: 26}}>Time at finish</Text></View>
                <View style={{width: 200, flexDirection: 'row', justifyContent: 'center'}}>
                        <TimeInput value={this.state.hours}
                            handleTextChange={this.handleHoursChange} />
                        <TimeInput value={this.state.minutes}
                            handleTextChange={this.handleMinutesChange} />
                        <TimeInput value={this.state.seconds}
            handleTextChange={this.handleSecondsChange} />
                </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    inputs: {
        flexDirection: 'row',
        width: 350
    },
    distance: {
        flexDirection: 'row',
        width: 350
    },
});
