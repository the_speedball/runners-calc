const SEPARATOR = ':';
const PADDING = '0';

const parseIntDefault = (v) => {
    const parsed = parseInt(v, 10);
    if (isNaN(parsed)) return 0;

    return parsed;
};

export const splitter = (time) => time.split(SEPARATOR);
export const joiner = (time) => time.join(SEPARATOR);
export const toInt = (timeUnits) => timeUnits.map(x => parseIntDefault(x));
export const toTimeStr = (timeUnits) => timeUnits.map(x => x.toString().padStart(2, PADDING));

export const toSeconds = function parseTimeIntoSeconds(time) {
    const [ hours, minutes, seconds ] = toInt(splitter(time));
    return hours * 3600 + minutes * 60 + seconds;
};

export const toTime = function expressSecondsInTime(seconds) {
    const hours = Math.floor(seconds / 3600);
    // compute number of minutes
    const minutes = Math.floor((seconds - (hours * 3600)) / 60);
    // remainder of division is seconds
    const secondsRemainder = seconds % 60;
    return joiner(toTimeStr([ hours, minutes, secondsRemainder ]));
};

export const calcAvg = function calculateAvgPace(distance, time) {
    // parse time into seconds
    const timeInS = toSeconds(time);
    // time in seconds by distance
    const kmInS = Math.floor(timeInS / distance);
    // return avg pace
    return toTime(kmInS);
};
