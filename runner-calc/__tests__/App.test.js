import { toSeconds, toTime, calcAvg } from '../TimeCalc';

test('toSeconds 04:00:00 equals 14400s', () => {
    expect(toSeconds('04:00:00')).toBe(14400);
});

test('toSeconds 00:50:00 equals 3000s', () => {
    expect(toSeconds('00:50:00')).toBe(3000);
});

test('toSeconds 00:00:35 equals 35s', () => {
    expect(toSeconds('00:00:35')).toBe(35);
});

test('toSeconds 09:02:01 equals 32521s', () => {
    expect(toSeconds('09:02:01')).toBe(32521);
});

test('toTime 32521s equals 09:02:01', () => {
    expect(toTime(32521)).toBe('09:02:01');
});

test('toTime 43s equals 00:00:43', () => {
    expect(toTime(43)).toBe('00:00:43');
});

test('toTime 141s equals 00:02:21', () => {
    expect(toTime(141)).toBe('00:02:21');
});

test('toTime 0s equals 00:00:00', () => {
    expect(toTime(0)).toBe('00:00:00');
});

test('calcAvg marathon for 04:00:00 equals ', () => {
    expect(calcAvg(42.195, '04:00:00')).toBe('00:05:41');
});

test('calcAvg marathon for 03:30:00 equals ', () => {
    expect(calcAvg(42.195, '03:30:00')).toBe('00:04:58');
});

test('calcAvg marathon for 03:00:00 equals ', () => {
    expect(calcAvg(42.195, '03:00:00')).toBe('00:04:15');
});

test('calcAvg marathon for 02:45:00 equals ', () => {
    expect(calcAvg(42.195, '02:45:00')).toBe('00:03:54');
});

test('calcAvg half-marathon for 02:00:00 equals ', () => {
    expect(calcAvg(21.0975, '02:00:00')).toBe('00:05:41');
});

test('calcAvg half-marathon for 01:45:00 equals ', () => {
    expect(calcAvg(21.0975, '01:45:00')).toBe('00:04:58');
});

test('calcAvg half-marathon for 01:30:00 equals ', () => {
    expect(calcAvg(21.0975, '01:30:00')).toBe('00:04:15');
});

test('calcAvg half-marathon for 01:15:00 equals ', () => {
    expect(calcAvg(21.0975, '01:15:00')).toBe('00:03:33');
});
